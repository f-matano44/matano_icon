// Copyright 2022 Fumiyoshi MATANO

size(512, 512);
background(135, 240, 240);
noFill();
strokeWeight(7);
rectMode(CENTER);
rect(208, 308, 240, 240);
rect(308, 208, 240, 240);
//rect(256, 256, 240, 240);
save("icon.png");

size(512, 512);
background(135, 240, 240);
noFill();
strokeWeight(28);
rectMode(CENTER);
rect(256, 256, 342, 342);
save("favicon.png");
